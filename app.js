//const express = require("express")

//const app = express()


//const port = 4001

//module.exports = app

//app.use(express.json())
//const userRouter = require('./routes/userRoutes')
//const viewRouter = require('./routes/viewRoutes')

//app.use('/api/v1/users', userRouter)
//app.use('/', viewRouter)

//express.static(root,[options])
//app.use(express.static(path.join(__dirname,'views')))


//const express = require("express")

//const app = express()

//module.exports = app
const express = require("express")
const path = require('path')
const app = express()
const userRouter = require('./routes/userRoutes')
const viewRouter = require('./routes/viewRoutes')

app.use(express.json())
app.use('/api/v1/users', userRouter)
app.use('/',viewRouter)

app.use(express.static(path.join(__dirname, 'views')))

/*Starting the server on port 4001.
const port = 4001
app.listen(port, ()=>{
    console.log('App running on port ${port} ..')
})  */
module.exports = app
