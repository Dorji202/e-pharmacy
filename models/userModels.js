const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')

const userSchema = new mongoose.Schema({
    name: {
        type:String,
        required:[true, 'Please tell your name!'], 
    },
    email:{
        type:String,
        required: [true, 'Please provide your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'Please provide a valid email'],
    },
    photo:{
        type: String,
        default: 'default.jpg',
    },
    role: {
        type:String,
        enum: ['user', 'sme', 'pharmacist','admin'],
        default:'user',
    },
    password:{
        type:String,
        required: [true, 'Please provide a password'],
        minlenght:8,
        select:false,

    },
    active:{
        type:Boolean,
        default:true,
        select:false,
    },
    passwordConfirm:{
        type: String,
        required: [true, 'Please confirm your password'],
        validate: {
            validator: function (e1){
                return e1 === this.password
            },
            message: 'Password are not the same',
        },
    },

    
})
userSchema.pre('save', async function (next){
    if (!this.isModified('password')) return next()
    this.password = await bcrypt.hash(this.password, 12)
    this.passwordConfirm = undefined
    next()
})
userSchema.methods.correctPassword = async function(
    candidatePassword,
    userPassword,

){
    return await bcrypt.compare(candidatePassword, userPassword)
}

const User = mongoose.model('User', userSchema)
module.exports = User
